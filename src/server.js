require('babel-register')({
    presets: ['react']
});
var express = require('express');
var app = express();
var React = require('react');
var ReactDOMServer = require('react-dom/server');
var PostsComponent = require('./components/posts_index');

app.get('/posts', function (req, res) {
    var html = ReactDOMServer.renderToString(
        React.createElement(PostsComponent)
    );
    res.send(html);
});

app.listen(8888, function () {
    console.log('Server started');
});